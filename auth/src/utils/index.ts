import jwt from 'express-jwt'
import { Express, Response, Request } from 'express'
import HttpStatus from 'http-status-codes'

import papyrus from '../stubs/papyrus'

export const jwtMdw = jwt({
  secret: process.env.JWT_SECRET,
  algorithms: ['RS256'],
})
