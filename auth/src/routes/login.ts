import express, { Request, Response, Router } from 'express'
import jwt from 'jsonwebtoken'

import { UserModel } from '../db'
import papyrus from '../stubs/papyrus'

const router: Router = express.Router()
const accessTokenSecret = process.env.JWT_SECRET

router.post(
  '/',
  async (req: Request, res: Response): Promise<any> => {
    const { email, password } = req.body

    const user = await UserModel.findOne({ email, password })

    if (user) {
      const accessToken = jwt.sign({ username: user.fullName, role: user.role }, accessTokenSecret)

      res.json({
        accessToken,
      })
    } else {
      res.send(papyrus.errors.wrongCredentials)
    }
  },
)

module.exports = router
