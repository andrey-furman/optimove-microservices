export default {
  errors: {
    missingPropertyError: (propName: string) => `miss the required property ${propName}`,
    pageNotFound: 'Sorry my firend. The page not found',
    wrongCredentials: "Wrong credentials",
    wrongParameter: (paramName: string) => `Wrong param data passed: ${paramName}`
  },
}
