import dotenv from 'dotenv'
import amqp, { Channel, Connection } from 'amqplib'

dotenv.config()

export class Bootstrap {
  conn: Connection
  constructor() {}

  async init() {
    this.conn = await amqp.connect(process.env.RABBIT_MQ_ENDPOINT)
  }

  async getChannel(): Promise<Channel> {
    return await this.conn.createChannel()
  }
}
