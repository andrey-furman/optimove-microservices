import { Channel } from 'amqplib'
import jwt from 'jsonwebtoken'

import { Bootstrap } from './bootstrap'

const bootstrap = new Bootstrap()

;(async () => {
  await bootstrap.init()
  const ch: Channel = await bootstrap.getChannel()
  await ch.assertExchange('common', 'direct', { durable: false })
  const q = await ch.assertQueue('auth', {
    durable: false,
  })
  // await ch.prefetch(1) // get the next message after acknowledge
  await ch.bindQueue(q.queue, 'common', 'auth')
  await ch.consume('auth', async msg => {
    const { email, password } = JSON.parse(msg.content.toString())
    const accessToken = jwt.sign({ email, role: 'guest' }, process.env.JWT_SECRET, {
      expiresIn: '5m',
    })
    console.log('routing key', msg.fields.routingKey)
    console.log('access token', accessToken)
    ch.ack(msg)
  })
})()
