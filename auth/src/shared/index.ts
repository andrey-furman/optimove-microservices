import { Document } from 'mongoose'

export interface User {
  email: string
  phoneNumber: number
  password: string
  authToken: string
  fullName: string
  role: string
  createdAt: Date
  updatedAt: Date
}

export interface Message {
  queue: string
  payload: { [key: string]: any }
}

export interface UserDocument extends Document, User {}
