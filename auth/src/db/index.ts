import mongoose, { Mongoose } from 'mongoose'

import { UserDocument } from '../shared'
import { UserSchema } from './models'

let models: any
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: true,
  useCreateIndex: true
})

export const UserModel = mongoose.model<UserDocument>('UserModel', UserSchema, 'users')
