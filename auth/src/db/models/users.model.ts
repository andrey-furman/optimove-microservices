import papyrus from '../../stubs/papyrus'
import { Schema } from 'mongoose'

export const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: papyrus.errors.missingPropertyError('Email'),
  },
  phoneNumber: { type: Number, unique: true, sparse: true },
  password: {
    type: String,
    required: papyrus.errors.missingPropertyError('Password'),
    minlength: 5,
    maxlength: 255,
  },
  authToken: {
    type: String,
  },
  fullName: { type: String, minlength: 6, maxlength: 60 },
  role: { type: String, enum: ['admin', 'client',], default: 'client' },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: null,
  },
})

UserSchema.pre('updateOne', (next: any) => {
  this.updatedAt = Date.now()
  return next()
})
