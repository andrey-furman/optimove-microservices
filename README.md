# Bootup instruction

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

1. You have to build a client part with command execution:

```sh
$ cd client; yarn build
```

2.  Next step you have to put all the necessary .env files to each microservices: client, auth, api.
3.  And finally go the the root project directory and run the next command:

```sh
docker-compose up --build
```

4. You have to make sure that the next ports are free:

- 80 - Client
- 3000 - API
- 3001 - Auth ms

5. Things which should be realized in perspective

- Errors handling system
- Message broker microservices communication
