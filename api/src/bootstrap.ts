import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import express, { Express } from 'express'
import helmet from 'helmet'

dotenv.config()

import { loadRoutes } from './helpers'

const app: Express = express()
const port = Number(process.env.PORT)
const host = process.env.HOST

// Set up middlewares
app.use(helmet())
app.use(bodyParser.urlencoded({ extended: true }))

// ☑️ jwt service integration

// Set up routes
;(async () => {
  await loadRoutes(app)
})()

app.listen(port, host, () => {
  console.log(`API runs on port: ${port}`)
})
