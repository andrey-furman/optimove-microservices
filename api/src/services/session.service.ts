import bluebird from "bluebird";
import redis from "redis";

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

type UserId = string | number;

export class SessionService {
  constructor(private redisClient) {}

  async getSession(uK: UserId): Promise<any> {
    let hK: string;
    if (!String(uK).match(/\w*:[\w\d]*/)) {
      hK = this.getUKey(uK);
    }
    return await this.redisClient.hgetallAsync(hK || uK);
  }

  public getUKey(id: UserId): string {
    return `${process.env.BOT_ALIAS}:${id}`;
  }

  async init(id: UserId): Promise<boolean> | never {
    const uK = this.getUKey(id);
    if (this.redisClient) {
      const session = await this.redisClient.hgetallAsync(uK);
      if (Boolean(session)) {
        await this.redisClient.delAsync(uK);
      }
      return await this.redisClient.hmsetAsync(uK, {});
    }
    throw new Error("Error on session init");
  }

  async flushSession() {
    const key = process.env.BOT_ALIAS;
    const flushResult = await this.redisClient.flushallAsync();
  }

  async destroy(id: UserId): Promise<boolean> {
    const uK = this.getUKey(id);
    const destroyResult = await this.redisClient.delAsync(uK);
    return Boolean(destroyResult);
  }

  async getData(id: UserId, requestedEntities: string[]): Promise<any> {
    if (!id) {
      return false;
    }
    const uK = this.getUKey(id);
    const session = await this.getSession(uK);
    if (!session) {
      return false;
    }
    const requestedData = Object.keys(session)
      .filter((sesKey) => requestedEntities.includes(sesKey))
      .reduce((obj, key) => {
        obj[key] = session[key];
        return obj;
      }, {});

    return requestedData;
  }

  async update(id: UserId, toUpdate: object): Promise<boolean> {
    const uK = this.getUKey(id);
    const session = await this.getSession(uK);
    const updateRes = await this.redisClient.hmsetAsync(uK, {
      ...session,
      ...toUpdate,
    });
    return Boolean(updateRes);
  }

  async checkout(id: UserId): Promise<boolean> {
    const uK = this.getUKey(id);

    return await this.redisClient.hmsetAsync(uK, {});
  }
}
