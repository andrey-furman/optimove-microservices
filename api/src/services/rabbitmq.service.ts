import amqp from 'amqplib'

import { Message } from '../shared'

export class RabbitMqService {
  private channel

  constructor() {
    this.init()
  }

  async init() {
    const conn = await amqp.connect(process.env.RABBIT_MQ_ENDPOINT)
    const channel = await conn.createChannel()
    await channel.assertExchange('common', 'direct', { durable: false })
    this.channel = channel
  }

  async publish(msg: Message): Promise<boolean> {
    const { exchange, queue, payload } = msg
    const x = exchange || 'common'

    return this.channel.publish(x, queue, Buffer.from(JSON.stringify(payload)))
  }

  async subscribe(queue: string) {
    const ch = this.channel
    ch.consume(
      queue,
      async msg => {
        console.log('✅', msg.content.toString())
      },
      {
        noAck: true,
      },
    )
  }
}

export const rmqService = new RabbitMqService()
