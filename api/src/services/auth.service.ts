import { rmqService, RabbitMqService } from '../services/rabbitmq.service'

export class AuthService {
  constructor(private rabbitService: RabbitMqService = rmqService) {}

  async login(email: string, password: string) {
    try {
      await this.rabbitService.publish({queue: 'auth', payload: {email, password}})
    } catch (err) {
      console.error(err)
    }
  }
}

export const authService = new AuthService()
