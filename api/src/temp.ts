// import amqp from 'amqplib'

// (async () => {  
//   const q = 'tasks'
//  const conn = amqp.connect(process.env.RABBIT_MQ_ENDPOINT)
//   // this function will be called when the connection is created
//   // `err` will contain the error object, if any errors occurred
//   // `conn` will contain the connection object

//   if (err != null) bail(err) // calls `bail` function if an error occurred when connecting
//   consumer(conn) // creates a consumer
//   publisher(conn) // creates a publisher
// })

// function bail(err) {
//   console.error(err)
//   process.exit(1)
// }

// // Publisher
// function publisher(conn) {
//   conn.createChannel(on_open) // creates a channel and call `on_open` when done
//   function on_open(err, ch) {
//     // this function will be called when the channel is created
//     // `err` will contain the error object, if any errors occurred
//     // `ch` will contain the channel object

//     if (err != null) bail(err) // calls `bail` function if an error occurred when creating the channel
//     ch.assertQueue(q) // asserts the queue exists
//     ch.sendToQueue(q, Buffer.from('something to do')) // sends a message to the queue
//   }
// }

// // Consumer
// function consumer(conn) {
//   var ok = conn.createChannel(on_open) // creates a channel and call `on_open` when done
//   function on_open(err, ch) {
//     // this function will be called when the channel is created
//     // `err` will contain the error object, if any errors occurred
//     // `ch` will contain the channel object

//     if (err != null) bail(err) // calls `bail` function if an error occurred when creating the channel
//     ch.assertQueue(q) // asserts the queue exists
//     ch.consume(q, function (msg) {
//       //consumes the queue
//       if (msg !== null) {
//         console.log('received message', msg.content.toString()) // writes the received message to the console
//         ch.ack(msg) // acknowledge that the message was received
//       }
//     })
//   }
// }
// })()
