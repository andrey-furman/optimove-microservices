import { Express, Response, Request } from 'express'
import path from 'path'
import { readdirSync } from 'fs'
import HttpStatus from 'http-status-codes'

import papyrus from '../stubs/papyrus'

// 🧭 Dynamic routes loading
export const loadRoutes = async (app: Express): Promise<void> => {
  const routesFolder = (source: string) => readdirSync(source)
  const routesAbsPath = path.join(__dirname, '..', 'routes')

  routesFolder(routesAbsPath).map(async routeRaw => {
    const routeParts = routeRaw.split('.')
    const routeName = routeParts[0]
    const routePath = path.join(__dirname, '..', 'routes', `${routeName}.js`)
    if (routeName !== 'index') {
      app.use(`/${routeName}`, require(routePath))
    }
  })

  setCommonRouteHandler(app)
}

const setCommonRouteHandler = (app): Express => {
  return app.all('*', async (req: Request, res: Response) => {
    return await res.status(HttpStatus.NOT_FOUND).send(papyrus.errors.pageNotFound)
  })
}
