import express, { Request, Response, Router } from 'express'

import { authService } from '../services'

const router: Router = express.Router()

try {
  setInterval(async () => {
    await authService.login('andrey@gmail.com', 'secret')
  }, 5000)
} catch (err) {
  console.log(err)
}

router.get(
  '/',
  async (req: Request, res: Response): Promise<Response> => {
    console.log('auth route works fine!')

    return await res.sendStatus(200)
  },
)

router.post(
  '/login',
  async (req: Request, res: Response): Promise<any> => {
    const { email, password } = req.query
    try {
      // const loginResult = await authService.login(String(email), String(password))
      // console.log('login result', loginResult)
    } catch (err) {
      console.error(err)
    }
  },
)

module.exports = router
