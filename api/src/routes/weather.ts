import express, { Request, Response, Router } from 'express'
import HttpStatus from 'http-status-codes'
import axios from 'axios'

import papyrus from '../stubs/papyrus'

const router: Router = express.Router()

router.get(
  '/',
  async (req: Request, res: Response): Promise<Response> => {
    const location: string = req.param('location')
    if (!location || typeof location !== 'string' || location.length < 2 || location.length > 60) {
      return await res
        .status(HttpStatus.NOT_ACCEPTABLE)
        .send(papyrus.errors.wrongParameter('location'))
    }
    const weatherData = await axios.get(`${process.env.WEATHER_ENDPOINT}current`, {
      params: { access_key: process.env.WEATHER_API_KEY, query: location },
    })

    const { temperature, wind_speed: wind, feelslike: feelsLike } = weatherData.data.current

    await res.sendStatus(200)
  },
)

module.exports = router
