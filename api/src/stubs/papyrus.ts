export default {
  errors: {
    pageNotFound: 'Sorry my firend. The page not found',
    wrongParameter: (paramName: string) => `Wrong param data passed: ${paramName}`,
    amqpWrongConnection: (errMessage: string) => `Error on amql connection ${errMessage}`,
    channelNotExists: (chName: string) =>
      `Unable to send message to the channel ${chName}. Channel doesn't exist`,
  },
}
