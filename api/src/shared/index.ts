export interface Message {
  exchange?: string
  queue: string
  payload: { [key: string]: any }
}